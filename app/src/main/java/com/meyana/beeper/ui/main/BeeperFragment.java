package com.meyana.beeper.ui.main;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.meyana.beeper.R;
import com.meyana.beeper.receiver.AlarmBroadcastReceiver;
import com.meyana.beeper.service.AlarmService;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import static com.meyana.beeper.receiver.AlarmBroadcastReceiver.TITLE;

public class BeeperFragment extends Fragment {
    private static final String TAG = "BeeperFragment";

    private static long ALARM_MILLIS_IN_FUTURE = 5000;
    private static long COUNTER_INTERVAL = 1000;
    private BeeperViewModel mViewModel;
    private TextView messageText;
    private Button beeperButton;
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private CountDownTimer countDownTimer;
    private long alarmMillisInFuture;

    public static BeeperFragment newInstance() {
        return new BeeperFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.beeper_fragment, container, false);
        messageText = view.findViewById(R.id.messageText);
        beeperButton = view.findViewById(R.id.beeperButton);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        try {
            long alarmMinutes = Long.parseLong(sharedPreferences.getString(getString(R.string.timerDurationKey), ""));
            if (alarmMinutes != 0) {
                alarmMillisInFuture = alarmMinutes * 60 * 1000;
            }
        } catch (NumberFormatException ex) {
            alarmMillisInFuture = ALARM_MILLIS_IN_FUTURE;
        }

        beeperButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickStartStop(view);
            }
        });
        createCountDownTimer(alarmMillisInFuture, COUNTER_INTERVAL);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(getActivity()).get(BeeperViewModel.class);
    }

    public void onClickStartStop(View view) {
        switch (view.getId()) {
            case R.id.beeperButton:
                mViewModel.toggleButton();
                if (mViewModel.isButtonUp()) {
                    onBeeperButtonUp(true);
                } else {
                    onBeeperButtonDown(view);
                }
                break;
            default:
        }
    }

    private void onBeeperButtonDown(View view) {
        createAlarm(view.getContext(), alarmMillisInFuture);
        beeperButton.setText("STOP");
        countDownTimer.start();
    }

    private void onBeeperButtonUp(boolean userInterrupted) {
        View view = getView();
        Context context = view.getContext();

        if (userInterrupted) {
            cancelAlarm(context);
            countDownTimer.cancel();
            Intent intentService = new Intent(context, AlarmService.class);
            context.stopService(intentService);
        }
        messageText.setText("Timer not started");
        beeperButton.setText("START");
    }

    private void cancelAlarm(Context context) {
        if (pendingIntent != null && alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
    }

    private void createAlarm(Context context, long millisInFuture) {
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmBroadcastReceiver.class);
        intent.putExtra(TITLE, "Mahendran's");
        pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() +
                        millisInFuture, pendingIntent);
    }

    private void createCountDownTimer(long millisinFuture, long countDownInterval) {
        countDownTimer = new CountDownTimer(millisinFuture, countDownInterval) {
            public void onTick(long millisUntilFinished) {
                String formattedTime = getFormattedTime(millisUntilFinished);
                messageText.setText("Time remaining: " + formattedTime);
            }

            public void onFinish() {
                messageText.setText("Ring Ring!");
            }
        };
    }

    private String getFormattedTime(long milliSeconds) {
        return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(milliSeconds),
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) % TimeUnit.MINUTES.toSeconds(1));
    }
}