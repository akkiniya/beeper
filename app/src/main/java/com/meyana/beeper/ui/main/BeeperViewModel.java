package com.meyana.beeper.ui.main;

import androidx.lifecycle.ViewModel;

public class BeeperViewModel extends ViewModel {
    private boolean isButtonUp;

    public BeeperViewModel() {
        super();
        isButtonUp = true;
    }

    public boolean isButtonUp() {
        return isButtonUp;
    }

    public void toggleButton() {
        isButtonUp = !isButtonUp;
    }
}