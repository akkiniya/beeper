package com.meyana.beeper;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.meyana.beeper.ui.main.BeeperFragment;
import com.meyana.beeper.ui.main.SettingsFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            BeeperFragment beeperFragment = new BeeperFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, beeperFragment)
                    .commitNow();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                onClickSettingsAction(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickSettingsAction(MenuItem item) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SettingsFragment settingsFragment = new SettingsFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, settingsFragment);
        transaction.addToBackStack(settingsFragment.getTag());
        transaction.commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just pop back stack.
        getSupportFragmentManager().popBackStack();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        return true;
    }
}