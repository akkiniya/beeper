package com.meyana.beeper;

import android.util.Log;

public class Logger {
    private static boolean IS_DEBUG_LOG_ENABLED = true;

    public static void log(String tag, String message) {
        if (IS_DEBUG_LOG_ENABLED) {
            Log.v(tag, message);
        }
    }
}
